<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Progress extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $color;
    public $percentage;
    public function __construct($color, $percentage)
    {
        $this->color = $color;
        $this->percentage = $percentage;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.progress');
    }
}
