@extends('layouts.template')

@section('tabTitle', 'When It')
@section('title', 'When It')
@section('whenit', 'cl0')

@section('left')
@endsection

@section('left1')
    <x-card :shadow="true" style="primary">
        <h2 class="ps-4 py-2 mb-4 fs-4 wht9 f600">New Event</h2>
        <div>
            <div class="d-flex mb-2">
                <input type="text" style="outline:none" class="border-0 r20 bg2 px-4 py-3 me-4 fs-5 wht9 f600 w-100"
                    placeholder="Title">
                <button type="button" class="f600 bg2 border-0 r20 wht6 px-4 py-3 ">Start</button>
                <span class="f400 wht8 fs-6 align-self-center px-3">to</span>
                <button type="button" class="f600 bg2 border-0 r20 wht6 px-4 py-3 ">End</button>
            </div>
            <textarea class="border-0 bg2 r20 my-4 p-4 fs-6 wht8 f600 w-100" style="outline:none" rows="3"
                placeholder="Description"></textarea>
            <div class="d-flex mb-3">
                <button type="button" class="f600 shdw bg2 border-0 r20 wht9 px-5 py-3 ">Add Event</button>
                <button type="button" class="f600 bg-transparent border-0 r20 wht6 px-4 py-3 ">Clear</button>
            </div>
        </div>
    </x-card>
@endsection

@section('right')
    <h2 class="ps-4 py-2 mt-5 mb-4 fs-4 wht9 f600">Today's Events</h2>
    <x-card class="me-3" :shadow="true" style="secondary">
        <div class="px-3 py-4">
            <div class="d-flex">
                <div class="align-self-center rounded-circle me-3"
                    style="width:1rem; height:1rem; border:3px solid #92DCFE">
                </div>
                <p class="align-self-center m-0 f400 fs-7 wht6">Vote Completed</p>
            </div>
            <div class="pt-3">
                <p class="fs-5 cl0 f600 mb-0">
                    Class Meeting
                </p>
                <p class="fs-7 wht6 f400 text">
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis dolorum id
                    dignissimos quod voluptatem, quam repellendus magni nemo. Accusantium ex
                    reiciendis
                    voluptatem nemo dicta molestiae ut iure nihil omnis cumque!
                </p>
                <div class="d-flex justify-content-end">
                    <p class="mb-0 fs-7 cl0 f600 ">24 Mei 2022 at 10.20</p>
                </div>
            </div>
        </div>
    </x-card>
@endsection

@section('bottom')
    <section class="pt-5 ">
        <h2 class="ps-5 py-2 fs-4 wht9 f600">Event List</h2>
        <section class="row py-5">
            <p class="wht8 fs-6 f600 ps-5 mb-1 ms-2">June 2022</p>
            <div class="col-4">
                <x-card class="me-3" :shadow="true" style="secondary">
                    <div class="px-3 py-4">
                        <div class="d-flex">
                            <div class="align-self-center rounded-circle me-3"
                                style="width:1rem; height:1rem; border:3px solid #ED5333"></div>
                            <p class="align-self-center m-0 f400 fs-7 wht6">Vote Uncompleted</p>
                        </div>
                        <div class="pt-3">
                            <p class="fs-5 wht9 f600 mb-0">
                                Class Meeting
                            </p>
                            <p class="fs-7 wht6 f400 text">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis dolorum id
                                dignissimos quod voluptatem, quam repellendus magni nemo. Accusantium ex
                                reiciendis
                                voluptatem nemo dicta molestiae ut iure nihil omnis cumque!
                            </p>
                            <div class="d-flex justify-content-end">
                                <p class="mb-0 fs-7 wht9 f600 ">24 Mei 2022 at 10.20</p>
                            </div>
                        </div>
                    </div>
                </x-card>
            </div>
            <div class="col-4">
                <x-card class="me-3" :shadow="true" style="secondary">
                    <div class="px-3 py-4">
                        <div class="d-flex">
                            <div class="align-self-center rounded-circle me-3"
                                style="width:1rem; height:1rem; border:3px solid #ED5333"></div>
                            <p class="align-self-center m-0 f400 fs-7 wht6">Vote Uncompleted</p>
                        </div>
                        <div class="pt-3">
                            <p class="fs-5 wht9 f600 mb-0">
                                Class Meeting
                            </p>
                            <p class="fs-7 wht6 f400 text">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis dolorum id
                                dignissimos quod voluptatem, quam repellendus magni nemo. Accusantium ex
                                reiciendis
                                voluptatem nemo dicta molestiae ut iure nihil omnis cumque!
                            </p>
                            <div class="d-flex justify-content-end">
                                <p class="mb-0 fs-7 wht9 f600 ">24 Mei 2022 at 10.20</p>
                            </div>
                        </div>
                    </div>
                </x-card>
            </div>
            <div class="col-4">
                <x-card class="me-3" :shadow="true" style="secondary">
                    <div class="px-3 py-4">
                        <div class="d-flex">
                            <div class="align-self-center rounded-circle me-3"
                                style="width:1rem; height:1rem; border:3px solid #ED5333"></div>
                            <p class="align-self-center m-0 f400 fs-7 wht6">Vote Uncompleted</p>
                        </div>
                        <div class="pt-3">
                            <p class="fs-5 wht9 f600 mb-0">
                                Class Meeting
                            </p>
                            <p class="fs-7 wht6 f400 text">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis dolorum id
                                dignissimos quod voluptatem, quam repellendus magni nemo. Accusantium ex
                                reiciendis
                                voluptatem nemo dicta molestiae ut iure nihil omnis cumque!
                            </p>
                            <div class="d-flex justify-content-end">
                                <p class="mb-0 fs-7 wht9 f600 ">24 Mei 2022 at 10.20</p>
                            </div>
                        </div>
                    </div>
                </x-card>
            </div>
            <div class="col-4">
                <x-card class="me-3" :shadow="true" style="secondary">
                    <div class="px-3 py-4">
                        <div class="d-flex">
                            <div class="align-self-center rounded-circle me-3"
                                style="width:1rem; height:1rem; border:3px solid #ED5333"></div>
                            <p class="align-self-center m-0 f400 fs-7 wht6">Vote Uncompleted</p>
                        </div>
                        <div class="pt-3">
                            <p class="fs-5 wht9 f600 mb-0">
                                Class Meeting
                            </p>
                            <p class="fs-7 wht6 f400 text">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis dolorum id
                                dignissimos quod voluptatem, quam repellendus magni nemo. Accusantium ex
                                reiciendis
                                voluptatem nemo dicta molestiae ut iure nihil omnis cumque!
                            </p>
                            <div class="d-flex justify-content-end">
                                <p class="mb-0 fs-7 wht9 f600 ">24 Mei 2022 at 10.20</p>
                            </div>
                        </div>
                    </div>
                </x-card>
            </div>
            <div class="col-4">
                <x-card class="me-3" :shadow="true" style="secondary">
                    <div class="px-3 py-4">
                        <div class="d-flex">
                            <div class="align-self-center rounded-circle me-3"
                                style="width:1rem; height:1rem; border:3px solid #ED5333"></div>
                            <p class="align-self-center m-0 f400 fs-7 wht6">Vote Uncompleted</p>
                        </div>
                        <div class="pt-3">
                            <p class="fs-5 wht9 f600 mb-0">
                                Class Meeting
                            </p>
                            <p class="fs-7 wht6 f400 text">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis dolorum id
                                dignissimos quod voluptatem, quam repellendus magni nemo. Accusantium ex
                                reiciendis
                                voluptatem nemo dicta molestiae ut iure nihil omnis cumque!
                            </p>
                            <div class="d-flex justify-content-end">
                                <p class="mb-0 fs-7 wht9 f600 ">24 Mei 2022 at 10.20</p>
                            </div>
                        </div>
                    </div>
                </x-card>
            </div>
            <div class="col-4">
                <x-card class="me-3" :shadow="true" style="secondary">
                    <div class="px-3 py-4">
                        <div class="d-flex">
                            <div class="align-self-center rounded-circle me-3"
                                style="width:1rem; height:1rem; border:3px solid #ED5333"></div>
                            <p class="align-self-center m-0 f400 fs-7 wht6">Vote Uncompleted</p>
                        </div>
                        <div class="pt-3">
                            <p class="fs-5 wht9 f600 mb-0">
                                Class Meeting
                            </p>
                            <p class="fs-7 wht6 f400 text">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis dolorum id
                                dignissimos quod voluptatem, quam repellendus magni nemo. Accusantium ex
                                reiciendis
                                voluptatem nemo dicta molestiae ut iure nihil omnis cumque!
                            </p>
                            <div class="d-flex justify-content-end">
                                <p class="mb-0 fs-7 wht9 f600 ">24 Mei 2022 at 10.20</p>
                            </div>
                        </div>
                    </div>
                </x-card>
            </div>
        </section>
        <section class="row py-5">
            <p class="wht8 fs-6 f600 ps-5 mb-1 ms-2">July 2022</p>
            <div class="col-4">
                <x-card class="me-3" :shadow="true" style="secondary">
                    <div class="px-3 py-4">
                        <div class="d-flex">
                            <div class="align-self-center rounded-circle me-3"
                                style="width:1rem; height:1rem; border:3px solid #ED5333"></div>
                            <p class="align-self-center m-0 f400 fs-7 wht6">Vote Uncompleted</p>
                        </div>
                        <div class="pt-3">
                            <p class="fs-5 wht9 f600 mb-0">
                                Class Meeting
                            </p>
                            <p class="fs-7 wht6 f400 text">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis dolorum id
                                dignissimos quod voluptatem, quam repellendus magni nemo. Accusantium ex
                                reiciendis
                                voluptatem nemo dicta molestiae ut iure nihil omnis cumque!
                            </p>
                            <div class="d-flex justify-content-end">
                                <p class="mb-0 fs-7 wht9 f600 ">24 Mei 2022 at 10.20</p>
                            </div>
                        </div>
                    </div>
                </x-card>
            </div>
            <div class="col-4">
                <x-card class="me-3" :shadow="true" style="secondary">
                    <div class="px-3 py-4">
                        <div class="d-flex">
                            <div class="align-self-center rounded-circle me-3"
                                style="width:1rem; height:1rem; border:3px solid #ED5333"></div>
                            <p class="align-self-center m-0 f400 fs-7 wht6">Vote Uncompleted</p>
                        </div>
                        <div class="pt-3">
                            <p class="fs-5 wht9 f600 mb-0">
                                Class Meeting
                            </p>
                            <p class="fs-7 wht6 f400 text">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis dolorum id
                                dignissimos quod voluptatem, quam repellendus magni nemo. Accusantium ex
                                reiciendis
                                voluptatem nemo dicta molestiae ut iure nihil omnis cumque!
                            </p>
                            <div class="d-flex justify-content-end">
                                <p class="mb-0 fs-7 wht9 f600 ">24 Mei 2022 at 10.20</p>
                            </div>
                        </div>
                    </div>
                </x-card>
            </div>
            <div class="col-4">
                <x-card class="me-3" :shadow="true" style="secondary">
                    <div class="px-3 py-4">
                        <div class="d-flex">
                            <div class="align-self-center rounded-circle me-3"
                                style="width:1rem; height:1rem; border:3px solid #ED5333"></div>
                            <p class="align-self-center m-0 f400 fs-7 wht6">Vote Uncompleted</p>
                        </div>
                        <div class="pt-3">
                            <p class="fs-5 wht9 f600 mb-0">
                                Class Meeting
                            </p>
                            <p class="fs-7 wht6 f400 text">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis dolorum id
                                dignissimos quod voluptatem, quam repellendus magni nemo. Accusantium ex
                                reiciendis
                                voluptatem nemo dicta molestiae ut iure nihil omnis cumque!
                            </p>
                            <div class="d-flex justify-content-end">
                                <p class="mb-0 fs-7 wht9 f600 ">24 Mei 2022 at 10.20</p>
                            </div>
                        </div>
                    </div>
                </x-card>
            </div>
            <div class="col-4">
                <x-card class="me-3" :shadow="true" style="secondary">
                    <div class="px-3 py-4">
                        <div class="d-flex">
                            <div class="align-self-center rounded-circle me-3"
                                style="width:1rem; height:1rem; border:3px solid #ED5333"></div>
                            <p class="align-self-center m-0 f400 fs-7 wht6">Vote Uncompleted</p>
                        </div>
                        <div class="pt-3">
                            <p class="fs-5 wht9 f600 mb-0">
                                Class Meeting
                            </p>
                            <p class="fs-7 wht6 f400 text">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis dolorum id
                                dignissimos quod voluptatem, quam repellendus magni nemo. Accusantium ex
                                reiciendis
                                voluptatem nemo dicta molestiae ut iure nihil omnis cumque!
                            </p>
                            <div class="d-flex justify-content-end">
                                <p class="mb-0 fs-7 wht9 f600 ">24 Mei 2022 at 10.20</p>
                            </div>
                        </div>
                    </div>
                </x-card>
            </div>
            <div class="col-4">
                <x-card class="me-3" :shadow="true" style="secondary">
                    <div class="px-3 py-4">
                        <div class="d-flex">
                            <div class="align-self-center rounded-circle me-3"
                                style="width:1rem; height:1rem; border:3px solid #ED5333"></div>
                            <p class="align-self-center m-0 f400 fs-7 wht6">Vote Uncompleted</p>
                        </div>
                        <div class="pt-3">
                            <p class="fs-5 wht9 f600 mb-0">
                                Class Meeting
                            </p>
                            <p class="fs-7 wht6 f400 text">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis dolorum id
                                dignissimos quod voluptatem, quam repellendus magni nemo. Accusantium ex
                                reiciendis
                                voluptatem nemo dicta molestiae ut iure nihil omnis cumque!
                            </p>
                            <div class="d-flex justify-content-end">
                                <p class="mb-0 fs-7 wht9 f600 ">24 Mei 2022 at 10.20</p>
                            </div>
                        </div>
                    </div>
                </x-card>
            </div>
            <div class="col-4">
                <x-card class="me-3" :shadow="true" style="secondary">
                    <div class="px-3 py-4">
                        <div class="d-flex">
                            <div class="align-self-center rounded-circle me-3"
                                style="width:1rem; height:1rem; border:3px solid #ED5333"></div>
                            <p class="align-self-center m-0 f400 fs-7 wht6">Vote Uncompleted</p>
                        </div>
                        <div class="pt-3">
                            <p class="fs-5 wht9 f600 mb-0">
                                Class Meeting
                            </p>
                            <p class="fs-7 wht6 f400 text">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis dolorum id
                                dignissimos quod voluptatem, quam repellendus magni nemo. Accusantium ex
                                reiciendis
                                voluptatem nemo dicta molestiae ut iure nihil omnis cumque!
                            </p>
                            <div class="d-flex justify-content-end">
                                <p class="mb-0 fs-7 wht9 f600 ">24 Mei 2022 at 10.20</p>
                            </div>
                        </div>
                    </div>
                </x-card>
            </div>
        </section>
    </section>
@endsection
