<div class="{{ $shadow }} {{ $style }} {{ $class }} my-3">
    <div class="p-2">
        {{ $slot }}
    </div>
</div>
