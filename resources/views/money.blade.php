@extends('layouts.template')

@section('tabTitle', 'Money')
@section('title', 'Money Flow')
@section('money', 'cl0')

@section('left')
    <section>
        <div class="d-flex justify-content-between">
            <div class="">
                <p class="f400 fs-6 m-0 wht9">Today Earnings</p>
                <h6 class="f600 fs-3 cl0">Rp 1.000.000</h6>
            </div>
            <div class="">
                <p class="f400 fs-6 m-0 wht9">Today Spendings</p>
                <h6 class="f600 fs-3 cl1">Rp 900.000</h6>
            </div>
        </div>
    </section>
@endsection

@section('left1')
    <x-card :shadow="true" style="primary">
        <h2 class="ps-4 py-2 mb-4 fs-4 wht9 f600">Last Transaction</h2>
        <div>
            <div class="d-flex mb-2">
                <div class="r20 d-flex bg2 p-3 me-4">
                    <label for="money" class="align-self-center px-3 fs-6 wht9 f600">Rp</label>
                    <input type="text" id="money" style="outline:none" class="border-0 bg-transparent fs-5 wht9 f600 w-100"
                        placeholder="0">
                </div>
                <button type="button" class="f600 bg2 border-0 r20  cl1 px-4 py-3 ">out</button>
                <button type="button" class="f600 bg-transparent border-0 r20 cl0 px-4 py-3 ">in</button>
            </div>
            <textarea class="border-0 bg2 r20 my-4 p-4 fs-6 wht8 f600 w-100" style="outline:none" rows="3"
                placeholder="Description"></textarea>
            <div class="d-flex mb-3">
                <button type="button" class="f600 bg2 shdw border-0 r20 wht9 px-5 py-3 ">Add Transaction</button>
                <button type="button" class="f600 bg-transparent border-0 r20 wht6 px-4 py-3 ">Clear</button>
            </div>
        </div>
    </x-card>
    <section class="pt-5 ">
        <h2 class="ps-5 py-2 mb-4 fs-4 wht9 f600">Transaction History</h2>
        <x-card :shadow="true" style="secondary" class="my-4">
            <div class="px-3 d-flex justify-content-between">
                <div class="d-flex">
                    <span class="align-self-center bi bi-arrow-down-circle fs-2 cl0"></span>
                    <div class="align-self-center ps-3">
                        <p class="f600 fs-6 wht9 mb-0 ">Breakfast</p>
                        <p class="f400 fs-7 wht6 mb-0 lt0">24 Mei 2022 at 10.20</p>
                    </div>
                </div>
                <p class="align-self-center m-0 f600 fs-5 wht9">Rp 100.000</p>
            </div>
        </x-card>
        <x-card :shadow="true" style="secondary" class="my-4">
            <div class="px-3 d-flex justify-content-between">
                <div class="d-flex">
                    <span class="align-self-center bi bi-arrow-up-circle fs-2 cl1"></span>
                    <div class="align-self-center ps-3">
                        <p class="f600 fs-6 wht9 mb-0 ">Breakfast</p>
                        <p class="f400 fs-7 wht6 mb-0 lt0">24 Apr 2022 at 10.20</p>
                    </div>
                </div>
                <p class="align-self-center m-0 f600 fs-5 wht9">Rp 100.000</p>
            </div>
        </x-card>
        <x-card :shadow="true" style="secondary" class="my-4">
            <div class="px-3 d-flex justify-content-between">
                <div class="d-flex">
                    <span class="align-self-center bi bi-arrow-down-circle fs-2 cl0"></span>
                    <div class="align-self-center ps-3">
                        <p class="f600 fs-6 wht9 mb-0 ">Breakfast</p>
                        <p class="f400 fs-7 wht6 mb-0 lt0">24 Apr 2022 at 10.20</p>
                    </div>
                </div>
                <p class="align-self-center m-0 f600 fs-5 wht9">Rp 100.000</p>
            </div>
        </x-card>
    </section>
@endsection

@section('right')
    <section class="p-5 mt-4">
        <p class="f600 fs-6 wht8 ps-2">This Month</p>
        <div class="d-flex">
            <img src="{{ asset('img/in.png') }}" alt="">
            <div class="ps-4 w-100">
                <div class="d-flex justify-content-between">
                    <p class="f400 fs-6 wht9">Earnings</p>
                    <H6 class="f600 fs-5 wht9">Rp 1.000.000</H6>
                </div>
                <x-progress color="#92DCFE" percentage="60"></x-progress>
            </div>
        </div>
        <br>
        <div class="d-flex">
            <img src="{{ asset('img/out.png') }}" alt="">
            <div class="ps-4 w-100">
                <div class="d-flex justify-content-between">
                    <p class="f400 fs-6 wht9">Spendings</p>
                    <H6 class="f600 fs-5 wht9">Rp 900.000</H6>
                </div>
                <x-progress color="#ED5333" percentage="85"></x-progress>
            </div>
        </div>
        <br>
        <div class="d-flex">
            <img src="{{ asset('img/wl.png') }}" alt="">
            <div class="ps-4 w-100">
                <div class="d-flex justify-content-between">
                    <p class="f400 fs-6 wht9">Wishlist</p>
                    <H6 class="f600 fs-5 wht9">Rp 900.000</H6>
                </div>
                <x-progress color="rgba(255,255,255,.7)" percentage="85"></x-progress>
            </div>
        </div>
    </section>
    <section class="m-4 p-2 bg2 r40 shdw">
        <div class="p-4">
            <div class="d-flex justify-content-between">
                <p class="f600 fs-6 wht6">Wishlist Total</p>
                <p class="f400 fs-6 wht6">5 months</p>
            </div>
            <h2 class="f600 wht9">Rp 10.000.000</h2>
        </div>
    </section>
    <section class="m-4 pt-3">
        <div class="d-flex justify-content-between px-4">
            <span class="align-self-center f600 fs-6 ms-2 wht9">Wishlist Item</span>
            <span class="align-self-center bi bi-plus wht9 fs-4"></span>
        </div>
        <x-card :shadow="false" style="secondary">
            <div class="d-flex justify-content-between py-1 px-2">
                <p class="align-self-center m-0 f600 fs-6 wht6">New laptop</p>
                <p class="align-self-center m-0 f400 fs-6 wht6">Rp 10.000.000</p>
            </div>
        </x-card>
        <x-card :shadow="false" style="secondary">
            <div class="d-flex justify-content-between py-1 px-2">
                <p class="align-self-center m-0 f600 fs-6 wht6">New laptop</p>
                <p class="align-self-center m-0 f400 fs-6 wht6">Rp 10.000.000</p>
            </div>
        </x-card>
        <x-card :shadow="false" style="secondary">
            <div class="d-flex justify-content-between py-1 px-2">
                <p class="align-self-center m-0 f600 fs-6 wht6">New laptop</p>
                <p class="align-self-center m-0 f400 fs-6 wht6">Rp 10.000.000</p>
            </div>
        </x-card>
    </section>
@endsection

@section('bottom')
@endsection
