<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Card extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $style = "";
    public $shadow = "shdw";
    public $class  = "";
    public function __construct($style, $shadow, $class = "")
    {
        if (!$shadow) $this->shadow = "";
        if ($style == "primary") $this->style = "bg1 p-4 r40";
        if ($style == "secondary") $this->style = "bg2 px-3 r20";
        $this->class = $class;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.card');
    }
}
